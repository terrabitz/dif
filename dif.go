package dif

type Nameable interface {
	GetName() string
}

type Creatable interface {
	Create()
}

type Updateable interface {
	Update()
}

type Readable interface {
	Get()
}

type Deleteable interface {
	Delete()
}
