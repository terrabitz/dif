package cmp

import (
	"errors"
	"reflect"
	"testing"

	"github.com/go-test/deep"
)

func TestCompare_SimpleValues(t *testing.T) {
	type Foo struct {
		S       string
		I       int
		B       bool
		private int
	}

	type args struct {
		l interface{}
		r interface{}
	}
	tests := []struct {
		name string
		args args
		want *Comparison
	}{
		{
			name: "No diff",
			args: args{
				l: &Foo{
					S: "hello",
					I: 42,
					B: true,
				},
				r: &Foo{
					S: "hello",
					I: 42,
					B: true,
				},
			},
			want: &Comparison{},
		},
		{
			name: "One diff",
			args: args{
				l: &Foo{
					S: "hello",
					I: 42,
					B: true,
				},
				r: &Foo{
					S: "hello",
					I: 420,
					B: true,
				},
			},
			want: &Comparison{
				Update: []ComparisonField{
					{
						OldValue: int64(42),
						NewValue: int64(420),
						Path:     []string{"I"},
					},
				},
			},
		},
		{
			name: "Multiple diff",
			args: args{
				l: &Foo{
					S: "goodbye",
					I: 42,
					B: true,
				},
				r: &Foo{
					S: "hello",
					I: 420,
					B: false,
				},
			},
			want: &Comparison{
				Update: []ComparisonField{
					{
						OldValue: "goodbye",
						NewValue: "hello",
						Path:     []string{"S"},
					},
					{
						OldValue: int64(42),
						NewValue: int64(420),
						Path:     []string{"I"},
					},
					{
						OldValue: true,
						NewValue: false,
						Path:     []string{"B"},
					},
				},
			},
		},
		{
			name: "Ignores unexported fields",
			args: args{
				l: &Foo{
					S:       "hello",
					I:       42,
					B:       true,
					private: 66,
				},
				r: &Foo{
					S:       "hello",
					I:       42,
					B:       true,
					private: 501,
				},
			},
			want: &Comparison{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Compare(tt.args.l, tt.args.r)
			if err != nil {
				t.Error(err)
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
			}
		})
	}
}

func TestCompare_NestedValues(t *testing.T) {
	type Sub struct {
		S string
	}

	type Foo struct {
		Sub Sub
	}

	type args struct {
		l interface{}
		r interface{}
	}
	tests := []struct {
		name string
		args args
		want *Comparison
	}{
		{
			name: "No diff",
			args: args{
				l: &Foo{
					Sub: Sub{
						S: "hello",
					},
				},
				r: &Foo{
					Sub: Sub{
						S: "hello",
					},
				},
			},
			want: &Comparison{},
		},
		{
			name: "One diff",
			args: args{
				l: &Foo{
					Sub: Sub{
						S: "goodbye",
					},
				},
				r: &Foo{
					Sub: Sub{
						S: "hello",
					},
				},
			},
			want: &Comparison{
				Update: []ComparisonField{
					{
						OldValue: "goodbye",
						NewValue: "hello",
						Path:     []string{"Sub", "S"},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Compare(tt.args.l, tt.args.r)
			if err != nil {
				t.Error(err)
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
			}
		})
	}
}

func TestCompare_Arrays(t *testing.T) {
	type Foo struct {
		S []string
	}

	type args struct {
		l interface{}
		r interface{}
	}
	tests := []struct {
		name string
		args args
		want *Comparison
	}{
		{
			name: "No diff",
			args: args{
				l: &Foo{
					S: []string{
						"foo",
						"bar",
					},
				},
				r: &Foo{
					S: []string{
						"foo",
						"bar",
					},
				},
			},
			want: &Comparison{},
		},
		{
			name: "One diff",
			args: args{
				l: &Foo{
					S: []string{
						"foo",
						"bar",
					},
				},
				r: &Foo{
					S: []string{
						"foo",
						"baz",
					},
				},
			},
			want: &Comparison{
				Remove: []ComparisonField{
					{
						OldValue: "bar",
						Path:     []string{"S"},
					},
				},
				Add: []ComparisonField{
					{
						NewValue: "baz",
						Path:     []string{"S"},
					},
				},
			},
		},
		{
			name: "Null array right",
			args: args{
				l: &Foo{
					S: []string{
						"foo",
						"bar",
					},
				},
				r: &Foo{},
			},
			want: &Comparison{
				Remove: []ComparisonField{
					{
						OldValue: "foo",
						Path:     []string{"S"},
					},
					{
						OldValue: "bar",
						Path:     []string{"S"},
					},
				},
			},
		},
		{
			name: "Null array left",
			args: args{
				l: &Foo{},
				r: &Foo{S: []string{
					"foo",
					"bar",
				}},
			},
			want: &Comparison{
				Add: []ComparisonField{
					{
						NewValue: "foo",
						Path:     []string{"S"},
					},
					{
						NewValue: "bar",
						Path:     []string{"S"},
					},
				},
			},
		},
		{
			name: "Empty array",
			args: args{
				l: &Foo{
					S: []string{},
				},
				r: &Foo{
					S: []string{
						"foo",
						"bar",
					},
				},
			},
			want: &Comparison{
				Add: []ComparisonField{
					{
						NewValue: "foo",
						Path:     []string{"S"},
					},
					{
						NewValue: "bar",
						Path:     []string{"S"},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Compare(tt.args.l, tt.args.r)
			if err != nil {
				t.Error(err)
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
			}
		})
	}
}

func TestCompare_ArraysNested(t *testing.T) {
	type Bar struct {
		S      string
		SubArr []string
	}
	type Foo struct {
		Sub []Bar
	}

	type args struct {
		l interface{}
		r interface{}
	}
	tests := []struct {
		name string
		args args
		want *Comparison
	}{
		{
			name: "No diff",
			args: args{
				l: &Foo{
					Sub: []Bar{
						{
							S: "foo",
						},
						{
							S: "bar",
						},
					},
				},
				r: &Foo{
					Sub: []Bar{
						{
							S: "foo",
						},
						{
							S: "bar",
						},
					},
				},
			},
			want: &Comparison{},
		},
		{
			name: "One diff",
			args: args{
				l: &Foo{
					Sub: []Bar{
						{
							S: "foo",
						},
						{
							S: "bar",
						},
					},
				},
				r: &Foo{
					Sub: []Bar{
						{
							S: "foo",
						},
						{
							S: "baz",
						},
					},
				},
			},
			want: &Comparison{
				Remove: []ComparisonField{
					{
						OldValue: Bar{
							S: "bar",
						},
						Path: []string{"Sub"},
					},
				},
				Add: []ComparisonField{
					{
						NewValue: Bar{
							S: "baz",
						},
						Path: []string{"Sub"},
					},
				},
			},
		},
		{
			name: "Doubly-nested arrays - No Diff",
			args: args{
				l: &Foo{
					Sub: []Bar{
						{
							SubArr: []string{"foo", "bar"},
						},
					},
				},
				r: &Foo{
					Sub: []Bar{
						{
							SubArr: []string{"foo", "bar"},
						},
					},
				},
			},
			want: &Comparison{},
		},
		{
			name: "Doubly-nested arrays - One Diff",
			args: args{
				l: &Foo{
					Sub: []Bar{
						{
							SubArr: []string{"foo", "bar"},
						},
					},
				},
				r: &Foo{
					Sub: []Bar{
						{
							SubArr: []string{"foo", "baz"},
						},
					},
				},
			},
			want: &Comparison{
				Remove: []ComparisonField{
					{
						OldValue: Bar{
							SubArr: []string{"foo", "bar"},
						},
						Path: []string{"Sub"},
					},
				},
				Add: []ComparisonField{
					{
						NewValue: Bar{
							SubArr: []string{"foo", "baz"},
						},
						Path: []string{"Sub"},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Compare(tt.args.l, tt.args.r)
			if err != nil {
				t.Error(err)
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
			}
		})
	}
}

func TestCompare_OrderedArrays(t *testing.T) {
	type args struct {
		l []string
		r []string
	}
	tests := []struct {
		name string
		args args
		want *Comparison
	}{
		{
			name: "No diff",
			args: args{
				l: []string{
					"foo",
					"bar",
				},
				r: []string{
					"foo",
					"bar",
				},
			},
			want: &Comparison{},
		},
		{
			name: "Add one",
			args: args{
				l: []string{
					"foo",
				},
				r: []string{
					"foo",
					"bar",
				},
			},
			want: &Comparison{
				Add: []ComparisonField{
					{
						NewValue: "bar",
						NewIndex: UintP(1),
						Path:     []string{},
					},
				},
			},
		},
		{
			name: "Remove one",
			args: args{
				l: []string{
					"foo",
					"bar",
				},
				r: []string{
					"bar",
				},
			},
			want: &Comparison{
				Remove: []ComparisonField{
					{
						OldValue: "foo",
						OldIndex: UintP(0),
						Path:     []string{},
					},
				},
				// TODO this should eventually be fixed so that "bar" is
				// considered to not have changed at all
				Update: []ComparisonField{
					{
						NewValue: "bar",
						OldValue: "bar",
						NewIndex: UintP(0),
						OldIndex: UintP(1),
						Path:     []string{},
					},
				},
			},
		},
		{
			name: "Move one",
			args: args{
				l: []string{
					"foo",
					"bar",
					"baz",
				},
				r: []string{
					"foo",
					"baz",
					"bar",
				},
			},
			want: &Comparison{
				// TODO this should eventually be fixed so that the resulting
				// comparison is one removal and one addition
				Update: []ComparisonField{
					{
						NewValue: "bar",
						OldValue: "bar",
						NewIndex: UintP(2),
						OldIndex: UintP(1),
						Path:     []string{},
					},
					{
						NewValue: "baz",
						OldValue: "baz",
						NewIndex: UintP(1),
						OldIndex: UintP(2),
						Path:     []string{},
					},
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := CompareSlices(tt.args.l, tt.args.r, true)
			if err != nil {
				t.Error(err)
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
			}
		})
	}
}

func TestCompare_Maps(t *testing.T) {
	type Foo struct {
		M map[string]string
	}

	type args struct {
		l interface{}
		r interface{}
	}
	tests := []struct {
		name string
		args args
		want *Comparison
	}{
		{
			name: "No diff",
			args: args{
				l: &Foo{
					M: map[string]string{
						"foo":  "bar",
						"foo2": "bar2",
					},
				},
				r: &Foo{
					M: map[string]string{
						"foo":  "bar",
						"foo2": "bar2",
					},
				},
			},
			want: &Comparison{},
		},
		{
			name: "Values are different",
			args: args{
				l: &Foo{
					M: map[string]string{
						"foo":  "bar",
						"foo2": "bar2",
					},
				},
				r: &Foo{
					M: map[string]string{
						"foo":  "bar",
						"foo2": "asdf",
					},
				},
			},
			want: &Comparison{
				Update: []ComparisonField{
					{
						OldValue: "bar2",
						NewValue: "asdf",
						Path:     []string{"M", "foo2"},
					},
				},
			},
		},
		{
			name: "Keys are different",
			args: args{
				l: &Foo{
					M: map[string]string{
						"foo":  "bar",
						"foo2": "bar2",
					},
				},
				r: &Foo{
					M: map[string]string{
						"foo":  "bar",
						"foo3": "bar2",
					},
				},
			},
			want: &Comparison{
				Add: []ComparisonField{
					{
						NewValue: "bar2",
						Path:     []string{"M", "foo3"},
					},
				},
				Remove: []ComparisonField{
					{
						OldValue: "bar2",
						Path:     []string{"M", "foo2"},
					},
				},
			},
		},
		{
			name: "Null map right",
			args: args{
				l: &Foo{
					M: map[string]string{
						"foo": "bar",
					},
				},
				r: &Foo{},
			},
			want: &Comparison{
				Remove: []ComparisonField{
					{
						OldValue: "bar",
						Path:     []string{"M", "foo"},
					},
				},
			},
		},
		{
			name: "Null map left",
			args: args{
				l: &Foo{},
				r: &Foo{
					M: map[string]string{
						"foo": "bar",
					},
				},
			},
			want: &Comparison{
				Add: []ComparisonField{
					{
						NewValue: "bar",
						Path:     []string{"M", "foo"},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Compare(tt.args.l, tt.args.r)
			if err != nil {
				t.Error(err)
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
			}
		})
	}
}

func TestCompare_MapsNested(t *testing.T) {
	type Sub struct {
		S string
		I int
	}

	type Foo struct {
		M map[string]Sub
	}

	type args struct {
		l interface{}
		r interface{}
	}
	tests := []struct {
		name string
		args args
		want *Comparison
	}{
		{
			name: "No diff",
			args: args{
				l: &Foo{
					M: map[string]Sub{
						"foo": {
							S: "foo",
							I: 42,
						},
						"foo2": {
							S: "foo2",
							I: 42,
						},
					},
				},
				r: &Foo{
					M: map[string]Sub{
						"foo": {
							S: "foo",
							I: 42,
						},
						"foo2": {
							S: "foo2",
							I: 42,
						},
					},
				},
			},
			want: &Comparison{},
		},
		{
			name: "One nested diff",
			args: args{
				l: &Foo{
					M: map[string]Sub{
						"foo": {
							S: "foo",
							I: 42,
						},
						"foo2": {
							S: "foo2",
							I: 42,
						},
					},
				},
				r: &Foo{
					M: map[string]Sub{
						"foo": {
							S: "foo",
							I: 42,
						},
						"foo2": {
							S: "foo2",
							I: 420,
						},
					},
				},
			},
			want: &Comparison{
				Update: []ComparisonField{
					{
						OldValue: int64(42),
						NewValue: int64(420),
						Path:     []string{"M", "foo2", "I"},
					},
				},
			},
		},
		{
			name: "Different Keys",
			args: args{
				l: &Foo{
					M: map[string]Sub{
						"foo": {
							S: "foo",
							I: 42,
						},
						"foo2": {
							S: "foo2",
							I: 42,
						},
					},
				},
				r: &Foo{
					M: map[string]Sub{
						"foo": {
							S: "foo",
							I: 42,
						},
						"foo3": {
							S: "foo2",
							I: 42,
						},
					},
				},
			},
			want: &Comparison{
				Add: []ComparisonField{
					{
						NewValue: Sub{
							S: "foo2",
							I: 42,
						},
						Path: []string{"M", "foo3"},
					},
				},
				Remove: []ComparisonField{
					{
						OldValue: Sub{
							S: "foo2",
							I: 42,
						},
						Path: []string{"M", "foo2"},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Compare(tt.args.l, tt.args.r)
			if err != nil {
				t.Error(err)
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
			}
		})
	}
}

func TestComparison_Len(t *testing.T) {
	type fields struct {
		Add            []ComparisonField
		Remove         []ComparisonField
		Update         []ComparisonField
		maxComparisons int
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name: "Empty",
			fields: fields{
				Add:    []ComparisonField{},
				Remove: []ComparisonField{},
				Update: []ComparisonField{},
			},
			want: 0,
		},
		{
			name: "Filled",
			fields: fields{
				Add: []ComparisonField{
					{},
					{},
				},
				Remove: []ComparisonField{
					{},
				},
				Update: []ComparisonField{
					{},
				},
			},
			want: 4,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Comparison{
				Add:            tt.fields.Add,
				Remove:         tt.fields.Remove,
				Update:         tt.fields.Update,
				maxComparisons: tt.fields.maxComparisons,
			}
			if got := c.Len(); got != tt.want {
				t.Errorf("Comparison.Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCompareWithLimit(t *testing.T) {
	type Foo struct {
		S string
		I int
		B bool
	}

	type args struct {
		l     interface{}
		r     interface{}
		limit int
	}
	tests := []struct {
		name    string
		args    args
		want    *Comparison
		wantErr bool
	}{
		{
			name: "Multiple diff",
			args: args{
				l: &Foo{
					S: "goodbye",
					I: 42,
					B: true,
				},
				r: &Foo{
					S: "hello",
					I: 420,
					B: false,
				},
				limit: 1,
			},
			want: &Comparison{
				Update: []ComparisonField{
					{
						OldValue: "goodbye",
						NewValue: "hello",
						Path:     []string{"S"},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := CompareWithLimit(tt.args.l, tt.args.r, tt.args.limit)
			if (err != nil) != tt.wantErr {
				t.Errorf("CompareWithLimit() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
			}
		})
	}
}

func TestTraverse(t *testing.T) {
	type Sub struct {
		B bool
		I int
	}
	type Foo struct {
		S       string
		Sub     Sub
		RefSub  *Sub
		Slice   []string
		M       map[string]string
		JSON    string `json:"json"`
		Omitted string `json:"omit,omitempty"`
	}

	type args struct {
		i    interface{}
		path []string
	}
	tests := []struct {
		name    string
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Simple Value",
			args: args{
				i: &Foo{
					S: "hello",
				},
				path: []string{"S"},
			},
			want: "hello",
		},
		{
			name: "Nested struct",
			args: args{
				i: &Foo{
					Sub: Sub{
						I: 42,
					},
				},
				path: []string{"Sub", "I"},
			},
			want: 42,
		},
		{
			name: "Nested struct pointer",
			args: args{
				i: &Foo{
					RefSub: &Sub{
						I: 42,
					},
				},
				path: []string{"RefSub", "I"},
			},
			want: 42,
		},
		{
			name: "Nested map",
			args: args{
				i: &Foo{
					M: map[string]string{
						"foo": "bar",
					},
				},
				path: []string{"M", "foo"},
			},
			want: "bar",
		},
		{
			name: "Search by JSON tag",
			args: args{
				i: &Foo{
					JSON: "foo",
				},
				path: []string{"json"},
			},
			want: "foo",
		},
		{
			name: "Search by JSON tag with omitempty",
			args: args{
				i: &Foo{
					Omitted: "foo",
				},
				path: []string{"omit"},
			},
			want: "foo",
		},
		{
			name: "Struct field doesn't exist",
			args: args{
				i:    &Foo{},
				path: []string{"NOTEXIST"},
			},
			wantErr: true,
		},
		{
			name: "Map key doesn't exist",
			args: args{
				i: &Foo{
					M: map[string]string{},
				},
				path: []string{"M", "foo"},
			},
			wantErr: true,
		},
		{
			name: "Map is nil",
			args: args{
				i:    &Foo{},
				path: []string{"M", "foo"},
			},
			wantErr: true,
		},
		{
			name: "Traverse through slice",
			args: args{
				i: &Foo{
					Slice: []string{"foo"},
				},
				path: []string{"Slice", "0"},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Traverse(tt.args.i, tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("Traverse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Traverse() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWalk(t *testing.T) {
	type Sub struct {
		S string
	}
	type Foo struct {
		S        string
		Sub      Sub
		RefSub   *Sub
		Slice    []string
		M        map[string]string
		private  string
		privateP *string
	}

	tests := []struct {
		name           string
		input          interface{}
		wantNumStrings int
		wantErr        bool
	}{
		{
			name: "No errors",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: []string{
					"foo",
					"foo",
				},
				M: map[string]string{
					"bar": "foo",
				},
			},
			wantNumStrings: 6,
			wantErr:        false,
		},
		{
			name: "Ignores nil maps and slices",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: nil,
				M:     nil,
			},
			wantNumStrings: 3,
			wantErr:        false,
		},
		{
			name: "Error in slice",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: []string{
					"error",
					"foo",
				},
				M: map[string]string{
					"bar": "foo",
				},
			},
			wantErr: true,
		},
		{
			name: "Error in map",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: []string{
					"foo",
					"foo",
				},
				M: map[string]string{
					"bar": "error",
				},
			},
			wantErr: true,
		},
		{
			name: "Ignores private fields",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: []string{
					"foo",
					"foo",
				},
				M: map[string]string{
					"bar": "foo",
				},
				private:  "error",
				privateP: StringP("error"),
			},
			wantNumStrings: 6,
			wantErr:        false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := []string{}
			getAllStrings := func(i interface{}) error {
				v := reflect.ValueOf(i)
				kind := v.Kind()
				if kind == reflect.String {
					str := v.String()
					if str == "error" {
						return errors.New("this is an error")
					}
					if str != "" {
						s = append(s, v.String())
					}
				}
				return nil
			}
			err := Walk(tt.input, getAllStrings)
			if (err != nil) != tt.wantErr {
				t.Errorf("Traverse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.wantNumStrings != 0 && tt.wantNumStrings != len(s) {
				t.Errorf("Expected %v strings, got %v", tt.wantNumStrings, len(s))
			}
		})
	}
}

func TestWalkAll(t *testing.T) {
	type Sub struct {
		S string
	}
	type Foo struct {
		S        string
		Sub      Sub
		RefSub   *Sub
		Slice    []string
		M        map[string]string
		private  string
		privateP *string
	}

	tests := []struct {
		name           string
		input          interface{}
		wantNumStrings int
		wantNumErrs    int
	}{
		{
			name: "No errors",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: []string{
					"foo",
					"foo",
				},
				M: map[string]string{
					"bar": "foo",
				},
			},
			wantNumStrings: 6,
			wantNumErrs:    0,
		},
		{
			name: "Ignores nil maps and slices",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: nil,
				M:     nil,
			},
			wantNumStrings: 3,
			wantNumErrs:    0,
		},
		{
			name: "Error in slice",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: []string{
					"error",
					"foo",
				},
				M: map[string]string{
					"bar": "error",
				},
			},
			wantNumStrings: 4,
			wantNumErrs:    2,
		},
		{
			name: "Ignores private fields",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: []string{
					"foo",
					"foo",
				},
				M: map[string]string{
					"bar": "foo",
				},
				private:  "error",
				privateP: StringP("error"),
			},
			wantNumStrings: 6,
			wantNumErrs:    0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := []string{}
			getAllStrings := func(i interface{}) error {
				v := reflect.ValueOf(i)
				kind := v.Kind()
				if kind == reflect.String {
					str := v.String()
					if str == "error" {
						return errors.New("this is an error")
					}
					if str != "" {
						s = append(s, v.String())
					}
				}
				return nil
			}
			errs := WalkAll(tt.input, getAllStrings)
			if len(errs) != tt.wantNumErrs {
				t.Errorf("Traverse() num errors = %v, wantErr %v", len(errs), tt.wantNumErrs)
				return
			}
			if tt.wantNumStrings != len(s) {
				t.Errorf("Expected %v strings, got %v", tt.wantNumStrings, len(s))
			}
		})
	}
}

func TestWalkAllPathError(t *testing.T) {
	type Sub struct {
		S string
	}
	type Foo struct {
		S        string
		Sub      Sub
		RefSub   *Sub
		Slice    []string
		M        map[string]string
		private  string
		privateP *string
	}
	myErr := errors.New("this is an error")

	tests := []struct {
		name           string
		input          interface{}
		wantNumStrings int
		wantErrs       []PathError
	}{
		{
			name: "No errors",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: []string{
					"foo",
					"foo",
				},
				M: map[string]string{
					"bar": "foo",
				},
			},
			wantNumStrings: 6,
		},
		{
			name: "Ignores nil maps and slices",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: nil,
				M:     nil,
			},
			wantNumStrings: 3,
		},
		{
			name: "Errors",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "error",
				},
				RefSub: &Sub{
					S: "error",
				},
				Slice: []string{
					"error",
					"foo",
				},
				M: map[string]string{
					"bar": "error",
				},
			},
			wantNumStrings: 2,
			wantErrs: []PathError{
				{
					Path: []string{"Sub", "S"},
					Err:  myErr,
				},
				{
					Path: []string{"RefSub", "S"},
					Err:  myErr,
				},
				{
					Path: []string{"Slice", "0"},
					Err:  myErr,
				},
				{
					Path: []string{"M", "bar"},
					Err:  myErr,
				},
			},
		},
		{
			name: "Ignores unexported fields",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: []string{
					"foo",
					"foo",
				},
				M: map[string]string{
					"bar": "foo",
				},
				private:  "error",
				privateP: StringP("error"),
			},
			wantNumStrings: 6,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := []string{}
			getAllStrings := func(i interface{}) error {
				v := reflect.ValueOf(i)
				kind := v.Kind()
				if kind == reflect.String {
					str := v.String()
					if str == "error" {
						return myErr
					}
					if str != "" {
						s = append(s, v.String())
					}
				}
				return nil
			}
			errs := WalkAllPathError(tt.input, getAllStrings)
			if tt.wantErrs != nil {
				if diff := deep.Equal(errs, tt.wantErrs); diff != nil {
					t.Error(diff)
				}
			}
			if tt.wantNumStrings != len(s) {
				t.Errorf("Expected %v strings, got %v", tt.wantNumStrings, len(s))
			}
		})
	}
}

func TestWalkAllPath(t *testing.T) {
	type Sub struct {
		S string
	}
	type Foo struct {
		S        string
		Sub      Sub
		RefSub   *Sub
		Slice    []string
		M        map[string]string
		private  string
		privateP *string
	}

	tests := []struct {
		name           string
		input          interface{}
		wantNumStrings int
		wantNumErrs    int
		wantErrorPaths [][]string
	}{
		{
			name: "No errors",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: []string{
					"foo",
					"foo",
				},
				M: map[string]string{
					"bar": "foo",
				},
			},
			wantNumStrings: 6,
			wantNumErrs:    0,
		},
		{
			name: "Ignores nil maps and slices",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: nil,
				M:     nil,
			},
			wantNumStrings: 3,
			wantNumErrs:    0,
		},
		{
			name: "Error in slice",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: []string{
					"error",
					"foo",
				},
				M: map[string]string{
					"bar": "error",
				},
			},
			wantNumStrings: 4,
			wantNumErrs:    2,
			wantErrorPaths: [][]string{
				{"Slice", "0"},
				{"M", "bar"},
			},
		},
		{
			name: "Ignores private fields",
			input: &Foo{
				S: "foo",
				Sub: Sub{
					S: "foo",
				},
				RefSub: &Sub{
					S: "foo",
				},
				Slice: []string{
					"foo",
					"foo",
				},
				M: map[string]string{
					"bar": "foo",
				},
				private:  "error",
				privateP: StringP("error"),
			},
			wantNumStrings: 6,
			wantNumErrs:    0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := []string{}
			errorPaths := [][]string{}
			getAllStrings := func(i interface{}, path []string) error {
				v := reflect.ValueOf(i)
				kind := v.Kind()
				if kind == reflect.String {
					str := v.String()
					if str == "error" {
						errorPaths = append(errorPaths, path)
						return errors.New("this is an error")
					}
					if str != "" {
						s = append(s, v.String())
					}
				}
				return nil
			}
			errs := WalkAllPath(tt.input, getAllStrings)
			if len(errs) != tt.wantNumErrs {
				t.Errorf("Traverse() num errors = %v, wantErr %v", len(errs), tt.wantNumErrs)
				return
			}
			if tt.wantNumStrings != len(s) {
				t.Errorf("Expected %v strings, got %v", tt.wantNumStrings, len(s))
			}
			if tt.wantErrorPaths != nil {
				if diff := deep.Equal(errorPaths, tt.wantErrorPaths); diff != nil {
					t.Error(diff)
				}
			}
		})
	}
}

func UintP(i uint) *uint {
	return &i
}

func StringP(s string) *string {
	return &s
}
