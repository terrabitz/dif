package cmp

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

// Comparable allows nodes to insert their own comparison logic. If this is
// implemented comparisons at the node's subtree will use this function, and
// appended with the comparison further up the tree
type Comparable interface {
	Compare(other interface{}) (*Comparison, error)
}

// Equal will perform a comparison between two objects and indicate whether there
// are any differences between the two. It's like Compare(), but faster since it
// will return on the first difference found.
func Equal(l, r interface{}) (bool, error) {
	c, err := CompareWithLimit(r, r, 1)
	if err != nil {
		return false, err
	}
	return c.IsEmpty(), nil
}

func equal(l, r reflect.Value) bool {
	c := &Comparison{
		maxComparisons: 1,
	}
	err := c.compare(l, r, []string{})
	return err == nil && c.IsEmpty()
}

// CompareWithLimit performs a recursive comparison of two arbitrary structs.
// However, once the number of differences equals the number set in 'limit', the
// recursive search will stop and the Comparison object will be returned. This
// is useful for cases like the Equal method, where all we care about is if
// there are any differences at all.
//
// If limit is set to 0, there is no limit on the number of diffs returned.
func CompareWithLimit(l, r interface{}, limit int) (*Comparison, error) {
	c := &Comparison{
		maxComparisons: limit,
	}
	lVal := reflect.ValueOf(l)
	rVal := reflect.ValueOf(r)

	if l == nil && r == nil {
		return c, nil
	} else if l == nil && r != nil {
		c.added([]string{}, r)
	} else if l != nil && r == nil {
		c.removed([]string{}, l)
	}

	if err := c.compare(lVal, rVal, []string{}); err != nil {
		return nil, err
	}
	return c, nil
}

// Compare will perform a deep comparison of two arbitrary values.
func Compare(l, r interface{}) (*Comparison, error) {
	return CompareWithLimit(l, r, 0)
}

// Comparison is the result of a comparison between two values. It contains the full
// list of everything added, removed, or updated between the two values.
type Comparison struct {
	Add    []ComparisonField `json:"add,omitempty"`
	Remove []ComparisonField `json:"remove,omitempty"`
	Update []ComparisonField `json:"update,omitempty"`

	maxComparisons int
}

func (c *Comparison) isFull() bool {
	return c.maxComparisons != 0 && c.Len() >= c.maxComparisons
}

//nolint:gocyclo,gocognit,funlen // This function is going to be complex until we can refactor it
func (c *Comparison) compare(l, r reflect.Value, stack []string) error {
	// Exit condition: we've amassed more diffs than our configured maximum
	if c.isFull() {
		return nil
	}
	// Check if one value is nil, e.g. T{x: *X} and T.x is nil
	if !l.IsValid() || !r.IsValid() {
		if l.IsValid() && !r.IsValid() {
			c.added(stack, l.Type())
		} else if !l.IsValid() && r.IsValid() {
			c.removed(stack, r.Type())
		}
		return nil
	}

	// If different types, they can't be equal
	lType := l.Type()
	rType := r.Type()
	if lType != rType {
		return errors.New("type mismatch")
	}

	if l.IsValid() && l.CanInterface() {
		if comparable, ok := l.Interface().(Comparable); ok {
			comparison, err := comparable.Compare(r.Interface())
			if err != nil {
				return err
			}
			c.merge(comparison, stack)
			return nil
		}
	}

	// Primitive https://golang.org/pkg/reflect/#Kind
	lKind := l.Kind()
	rKind := r.Kind()

	// Do a and b have underlying elements? Yes if they're ptr or interface.
	lElem := lKind == reflect.Ptr || lKind == reflect.Interface
	rElem := rKind == reflect.Ptr || rKind == reflect.Interface

	if lElem || rElem {
		if lElem {
			l = l.Elem()
		}
		if rElem {
			r = r.Elem()
		}
		return c.compare(l, r, stack)
	}

	switch lKind {
	case reflect.Struct:
		/*
			The variables are structs like:
				type T struct {
					FirstName string
					LastName  string
				}
			Type = <pkg>.T, Kind = reflect.Struct
			Iterate through the fields (FirstName, LastName), recurse into their values.
		*/

		for i := 0; i < l.NumField(); i++ {
			if lType.Field(i).Tag.Get("dif") == "-" {
				continue // field wants to be ignored
			}

			newStack := append(stack, lType.Field(i).Name) // push field name to buff

			// Get the Value for each field, e.g. FirstName has Type = string,
			// Kind = reflect.String.
			lf := l.Field(i)
			rf := r.Field(i)

			// Ignore unexported fields
			if !lf.IsValid() || !lf.CanInterface() {
				continue
			}

			// Recurse to compare the field values
			if err := c.compare(lf, rf, newStack); err != nil {
				return err
			}
		}
	case reflect.Map:
		/*
			The variables are maps like:
				map[string]int{
					"foo": 1,
					"bar": 2,
				}
			Type = map[string]int, Kind = reflect.Map
			Or:
				type T map[string]int{}
			Type = <pkg>.T, Kind = reflect.Map
			Iterate through the map keys (foo, bar), recurse into their values.
		*/

		if l.IsNil() || r.IsNil() {
			if l.IsNil() && r.Len() != 0 {
				l = reflect.MakeMapWithSize(l.Type(), 0)
			} else if l.Len() != 0 && r.IsNil() {
				r = reflect.MakeMapWithSize(r.Type(), 0)
			}
		}

		if l.Pointer() == r.Pointer() {
			return nil
		}

		for _, key := range l.MapKeys() {
			// We make the assumption that keys will ONLY be strings
			if key.Kind() != reflect.String {
				return errors.New("map keys must be strings")
			}
			newStack := append(stack, key.String())

			lVal := l.MapIndex(key)
			rVal := r.MapIndex(key)
			if rVal.IsValid() {
				err := c.compare(lVal, rVal, newStack)
				if err != nil {
					return err
				}
			} else {
				c.removed(newStack, lVal.Interface())
			}

			if c.isFull() {
				return nil
			}
		}

		for _, key := range r.MapKeys() {
			if key.Kind() != reflect.String {
				return errors.New("map keys must be strings")
			}
			newStack := append(stack, key.String())

			if lVal := l.MapIndex(key); lVal.IsValid() {
				continue
			}

			c.added(newStack, r.MapIndex(key).Interface())
			if c.isFull() {
				return nil
			}
		}
	case reflect.Slice:
		subComparison, err := compareSlices(l, r, false)
		if err != nil {
			return err
		}
		c.merge(subComparison, stack)
	case reflect.Bool:
		if r.Bool() != l.Bool() {
			c.updated(stack, l.Bool(), r.Bool())
		}
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		if l.Int() != r.Int() {
			c.updated(stack, l.Int(), r.Int())
		}
	case reflect.String:
		if l.String() != r.String() {
			c.updated(stack, l.String(), r.String())
		}
	default:
		return errors.New("could not compare objects")
	}
	return nil
}

// CompareSlices allows for the specific use case of comparing slices in either
// an ordered or unordered fashion. If the slices are compared in an ordered
// fashion, the index of items will be considered in the equality condition. If
// an item is contained within both arrays but is at a different index, it will
// be considered to have moved.
//
// Note, the algorithm is currently naive in that it will simply compare indexes
// rather than using a more sophisticated diff approach (e.g. Myers Diff).As
// such, it's likely to be too verbose.
func CompareSlices(l, r interface{}, isOrdered bool) (*Comparison, error) {
	return compareSlices(reflect.ValueOf(l), reflect.ValueOf(r), isOrdered)
}

//nolint:gocyclo,gocognit,funlen // This function is going to be complex until we can refactor it
func compareSlices(l, r reflect.Value, isOrdered bool) (*Comparison, error) {
	if l.Kind() != reflect.Slice || r.Kind() != reflect.Slice {
		return nil, errors.New("compareSlices can only be used on slice types")
	}

	c := &Comparison{}
	path := []string{}

	// If either slice is nil, consider it to be of length 0
	if l.IsNil() && r.Len() != 0 {
		l = reflect.MakeSlice(l.Type(), 0, 0)
	} else if l.Len() != 0 && r.IsNil() {
		r = reflect.MakeSlice(r.Type(), 0, 0)
	}

	lLen := l.Len()
	rLen := r.Len()

	// If the slices point to the same thing and have the same length, we can
	// assume they're equivalent
	if l.Pointer() == r.Pointer() && lLen == rLen {
		return c, nil
	}

	for i := 0; i < lLen; i++ {
		foundMatch := false
		matchItemIndex := 0
		var matchItem interface{}

		item := l.Index(i)
		for j := 0; j < rLen; j++ {
			// Look at each value in r to see if we have a match for the ith
			// item in l

			foundMatch = equal(item, r.Index(j))
			if foundMatch {
				matchItemIndex = j
				matchItem = r.Index(j).Interface()
				break
			}
		}
		if !foundMatch {
			// This means we looked through every item on the other side
			// without finding a match
			if isOrdered {
				c.removedFromIndex(path, item.Interface(), uint(i))
			} else {
				c.removed(path, item.Interface())
			}
		} else if isOrdered && i != matchItemIndex {
			c.movedIndex(path, item.Interface(), matchItem, uint(i), uint(matchItemIndex))
		}
		if c.isFull() {
			return c, nil
		}
	}

	// Do the same thing, just on the other side
	for j := 0; j < rLen; j++ {
		foundMatch := false
		item := r.Index(j)
		for i := 0; i < lLen; i++ {
			foundMatch = equal(item, l.Index(i))
			if foundMatch {
				break
			}
		}
		if !foundMatch {
			if isOrdered {
				c.addedAtIndex(path, item.Interface(), uint(j))
			} else {
				c.added(path, item.Interface())
			}
		}
		if c.isFull() {
			return c, nil
		}
	}

	return c, nil
}

// Len returns the total number of differences found
func (c *Comparison) Len() int {
	return len(c.Add) + len(c.Update) + len(c.Remove)
}

// IsEmpty reports if there are any differences found
func (c *Comparison) IsEmpty() bool {
	return c.Len() == 0
}

func (c *Comparison) added(path []string, newObject interface{}) {
	field := ComparisonField{
		NewValue: newObject,
		Path:     path,
	}
	c.Add = append(c.Add, field)
}

func (c *Comparison) addedAtIndex(path []string, newObject interface{}, index uint) {
	field := ComparisonField{
		NewValue: newObject,
		Path:     path,
		NewIndex: &index,
	}
	c.Add = append(c.Add, field)
}

func (c *Comparison) removed(path []string, oldObject interface{}) {
	field := ComparisonField{
		OldValue: oldObject,
		Path:     path,
	}
	c.Remove = append(c.Remove, field)
}

func (c *Comparison) removedFromIndex(path []string, oldObject interface{}, index uint) {
	field := ComparisonField{
		OldValue: oldObject,
		Path:     path,
		OldIndex: &index,
	}
	c.Remove = append(c.Remove, field)
}

func (c *Comparison) updated(path []string, oldObject, newObject interface{}) {
	field := ComparisonField{
		OldValue: oldObject,
		NewValue: newObject,
		Path:     path,
	}
	c.Update = append(c.Update, field)
}

func (c *Comparison) movedIndex(path []string, oldObject, newObject interface{}, oldIndex, newIndex uint) {
	field := ComparisonField{
		OldValue: oldObject,
		NewValue: newObject,
		Path:     path,
		NewIndex: &newIndex,
		OldIndex: &oldIndex,
	}
	c.Update = append(c.Update, field)
}

func (c *Comparison) merge(other *Comparison, path []string) {
	for _, added := range other.Add {
		added.Path = append(path, added.Path...)
		c.Add = append(c.Add, added)
	}

	for _, updated := range other.Update {
		updated.Path = append(path, updated.Path...)
		c.Update = append(c.Update, updated)
	}

	for _, removed := range other.Remove {
		removed.Path = append(path, removed.Path...)
		c.Remove = append(c.Remove, removed)
	}
}

// ComparisonField represents the change that occurs at a specific level of the
// compared values.
type ComparisonField struct {
	// New value contains the new value of item item. This will only be present
	// if the node is being updated or added
	NewValue interface{} `json:"new_value,omitempty"`

	// OldValue contains the old value of the item. This will only be present if
	// the node is updated or removed.
	OldValue interface{} `json:"old_value,omitempty"`

	// Path contains the full path to the node within the tree. Each string in
	// the path indicates one of:
	// 1. A struct field name
	// 2. A map key
	Path []string `json:"path,omitempty"`

	// NewIndex indicates the new index of an ordered node. This field will only
	// be non-nil if the node is ordered and is either being moved or inserted.
	NewIndex *uint `json:"new_index,omitempty"`

	// OldIndex indicates the old index of an ordered node. This field will only
	// be non-nil if the node is ordered and is either being moved or removed.
	OldIndex *uint `json:"old_index,omitempty"`
}

// Traverse take in an arbitrary object, and returns whatever object
// resides at the path given by the path variable. If the given path doesn't
// exist or isn't viable, an error is returned. The path may specific
// specific fields or map keys to use. The assumption is that any map keys
// must be strings.
//nolint:gocognit // This is as simplified as I can make it right now
func Traverse(i interface{}, path []string) (interface{}, error) {
	traversed := []string{}
	v := reflect.ValueOf(i)

	for {
		// Exit condition: path is complete, so return current object
		if len(path) == 0 {
			return v.Interface(), nil
		}
		vKind := v.Kind()
		hasElem := vKind == reflect.Ptr || vKind == reflect.Interface
		if hasElem {
			v = v.Elem()
			continue
		}
		switch vKind {
		case reflect.Struct:

			fieldName, newPath := path[0], path[1:]
			field := v.FieldByName(fieldName)
			if !field.IsValid() {
				// Try to find matching JSON struct tag
				field = findFieldByJSONTag(v, fieldName)
			}
			if !field.IsValid() {
				return nil, fmt.Errorf("no field called '%s' found at path '%s'", fieldName, strings.Join(traversed, "/"))
			}

			v = field
			traversed = append(traversed, fieldName)
			path = newPath
		case reflect.Map:
			key, newPath := path[0], path[1:]
			if v.IsNil() {
				return nil, errors.New("null map found")
			}
			value := v.MapIndex(reflect.ValueOf(key))
			if !value.IsValid() {
				return nil, fmt.Errorf("index '%s' not found at path '%s'", key, strings.Join(traversed, "/"))
			}
			traversed = append(traversed, key)
			path = newPath
			v = value
		default:
			return nil, errors.New("could not traverse object")
		}
	}
}

func findFieldByJSONTag(v reflect.Value, match string) reflect.Value {
	vType := v.Type()
	for i := 0; i < vType.NumField(); i++ {
		field := v.Type().Field(i)
		jsonTag := field.Tag.Get("json")
		if jsonTag == "" {
			continue
		}

		jsonName := strings.Split(jsonTag, ",")[0]
		if match == jsonName {
			return v.FieldByIndex([]int{i})
		}
	}
	return reflect.Value{}
}

type WalkFunc func(interface{}) error

// Walk will recursively walk through an arbitrary input, and execute the given
// function for the given interface and all its children.

// If an error is returned from the given function, all walking is stopped and
// the error is returned.
func Walk(i interface{}, f WalkFunc) error {
	v := reflect.ValueOf(i)

	return walk(v, f)
}

//nolint:gocognit // This is as simple as I can make it
func walk(v reflect.Value, f WalkFunc) error {
	if !v.IsValid() || !v.CanInterface() {
		return nil
	}
	if err := f(v.Interface()); err != nil {
		return err
	}

	vKind := v.Kind()
	hasElem := vKind == reflect.Ptr || vKind == reflect.Interface
	if hasElem {
		v = v.Elem()
		return walk(v, f)
	}
	switch vKind {
	case reflect.Struct:
		for i := 0; i < v.NumField(); i++ {
			field := v.Field(i)
			if err := walk(field, f); err != nil {
				return err
			}
		}
	case reflect.Map:
		if v.IsNil() {
			return nil
		}
		for _, key := range v.MapKeys() {
			value := v.MapIndex(key)
			if err := walk(value, f); err != nil {
				return err
			}
		}
	case reflect.Slice:
		if v.IsNil() {
			return nil
		}
		for i := 0; i < v.Len(); i++ {
			value := v.Index(i)
			if err := walk(value, f); err != nil {
				return err
			}
		}
	default:
		// Exit condition: nothing more to do
		return nil
	}
	return nil
}

// WalkAll is similar to Walk, but will not immediately stop after
// receiving an error. Rather, it will append it to an error array
// and return the combined result.
func WalkAll(i interface{}, f WalkFunc) []error {
	v := reflect.ValueOf(i)

	return walkAll(v, f)
}

//nolint:gocognit // This is as simple as I can make it
func walkAll(v reflect.Value, f WalkFunc) []error {
	errs := []error{}
	if !v.IsValid() || !v.CanInterface() {
		return errs
	}
	if err := f(v.Interface()); err != nil {
		errs = append(errs, err)
	}

	vKind := v.Kind()
	hasElem := vKind == reflect.Ptr || vKind == reflect.Interface
	if hasElem {
		v = v.Elem()
		errs = append(errs, walkAll(v, f)...)
	}
	switch vKind {
	case reflect.Struct:
		for i := 0; i < v.NumField(); i++ {
			field := v.Field(i)
			errs = append(errs, walkAll(field, f)...)
		}
	case reflect.Map:
		if v.IsNil() {
			return nil
		}
		for _, key := range v.MapKeys() {
			value := v.MapIndex(key)
			errs = append(errs, walkAll(value, f)...)
		}
	case reflect.Slice:
		if v.IsNil() {
			return nil
		}
		for i := 0; i < v.Len(); i++ {
			value := v.Index(i)
			errs = append(errs, walkAll(value, f)...)
		}
	}
	return errs
}

// A PathError is a type of error used to indicate an error occurring while applying
// a function recursively through a value.
type PathError struct {
	// Path lists the path at which the error occurred.
	Path []string

	// Err contains the inner error
	Err error
}

// Error simply returns whatever string is contained by the inner error
func (p *PathError) Error() string {
	return p.Err.Error()
}

// WalkAllPathError walks through a given object, and will apply function f
// recursively through the given struct. If f returns an error, that error will
// be returned as well as the struct path at which that error occurred.
func WalkAllPathError(i interface{}, f WalkFunc) []PathError {
	v := reflect.ValueOf(i)

	return walkAllPathError(v, f, []string{})
}

//nolint:gocognit // This is as simple as I can make it
func walkAllPathError(v reflect.Value, f WalkFunc, path []string) []PathError {
	errs := []PathError{}
	if !v.IsValid() || !v.CanInterface() {
		return errs
	}
	if err := f(v.Interface()); err != nil {
		pathError := PathError{
			Path: path,
			Err:  err,
		}
		errs = append(errs, pathError)
	}

	vKind := v.Kind()
	hasElem := vKind == reflect.Ptr || vKind == reflect.Interface
	if hasElem {
		v = v.Elem()
		errs = append(errs, walkAllPathError(v, f, path)...)
	}
	switch vKind {
	case reflect.Struct:
		for i := 0; i < v.NumField(); i++ {
			field := v.Field(i)
			fieldName := v.Type().Field(i).Name
			newPath := append(path, fieldName)
			errs = append(errs, walkAllPathError(field, f, newPath)...)
		}
	case reflect.Map:
		if v.IsNil() {
			return nil
		}
		for _, key := range v.MapKeys() {
			if key.Kind() != reflect.String {
				panic("map keys must be strings")
			}
			value := v.MapIndex(key)
			newPath := append(path, key.String())
			errs = append(errs, walkAllPathError(value, f, newPath)...)
		}
	case reflect.Slice:
		if v.IsNil() {
			return nil
		}
		for i := 0; i < v.Len(); i++ {
			value := v.Index(i)
			newPath := append(path, strconv.Itoa(i))
			errs = append(errs, walkAllPathError(value, f, newPath)...)
		}
	}
	return errs
}

type WalkPathFunc func(i interface{}, path []string) error

// WalkAll is similar to Walk, but will not immediately stop after
// receiving an error. Rather, it will append it to an error array
// and return the combined result.
func WalkAllPath(i interface{}, f WalkPathFunc) []error {
	v := reflect.ValueOf(i)

	return walkAllPath(v, f, []string{})
}

//nolint:gocognit // This is as simple as I can make it
func walkAllPath(v reflect.Value, f WalkPathFunc, path []string) []error {
	errs := []error{}
	if !v.IsValid() || !v.CanInterface() {
		return errs
	}
	if err := f(v.Interface(), path); err != nil {
		errs = append(errs, err)
	}

	vKind := v.Kind()
	hasElem := vKind == reflect.Ptr || vKind == reflect.Interface
	if hasElem {
		v = v.Elem()
		errs = append(errs, walkAllPath(v, f, path)...)
	}
	switch vKind {
	case reflect.Struct:
		for i := 0; i < v.NumField(); i++ {
			field := v.Field(i)
			fieldName := v.Type().Field(i).Name
			newPath := append(path, fieldName)
			errs = append(errs, walkAllPath(field, f, newPath)...)
		}
	case reflect.Map:
		if v.IsNil() {
			return nil
		}
		for _, key := range v.MapKeys() {
			if key.Kind() != reflect.String {
				panic("map keys must be strings")
			}
			value := v.MapIndex(key)
			newPath := append(path, key.String())
			errs = append(errs, walkAllPath(value, f, newPath)...)
		}
	case reflect.Slice:
		if v.IsNil() {
			return nil
		}
		for i := 0; i < v.Len(); i++ {
			value := v.Index(i)
			newPath := append(path, strconv.Itoa(i))
			errs = append(errs, walkAllPath(value, f, newPath)...)
		}
	}
	return errs
}
