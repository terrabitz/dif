test:
	go test ./...

lint:
	golangci-lint run

tidy: 
	./build/tidy.sh

release: test lint tidy
	standard-version