# dif

This is a deep comparison module that is heavily based on
<github.com/go-test/deep>. The only difference is that, rather than reporting
differences as a simple list of strings, this attempts to give a more accurate
depiction of exactly what was added, updated, or removed between two arbitrary structs.

## Usage

{{ usage }}

## Meta

Trevor Taubitz – [@terrabitz](https://twitter.com/terrabitz)

Distributed under the MIT license. See `LICENSE` for more information.

[https://gitlab.com/terrabitz/](https://gitlab.com/terrabitz)

## Acknowledgements

**github.com/go-test/deep**

- Copyright 2015-2017 Daniel Nichter
- MIT License (see <https://github.com/go-test/deep/blob/master/LICENSE>)
